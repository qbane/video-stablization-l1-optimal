# Usage

`opencv-python` requires pip 19.3+:

```
pip install --upgrade pip
pip install -r requirements.txt
```

Invoking:

```
python L1optimal.py --help
python L1optimal.py -i video.mp4 --trajPlot --rect
```

The stablized video will be put at `results/` and trajectories,
if enabled, is put at `plots/`. You need to make sure these folder
exists before invoking commands.
